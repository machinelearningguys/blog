import keras
from keras.models import Sequential
from keras.layers import Conv2D, Activation, GlobalAvgPool2D, MaxPooling2D, Dropout
from keras.datasets import cifar10
import matplotlib.pyplot as plt
from keras.utils import to_categorical
import pickle

#
# Loading CIFAR-10
#

(x_train, y_label_train), (x_test, y_label_test) = cifar10.load_data()

y_train = to_categorical(y_label_train, 10)
y_test = to_categorical(y_label_test, 10)


#
# Model
#

model = Sequential()

model.add(Conv2D(48, 3, padding='same', activation='relu', input_shape=(32, 32, 3)))
model.add(Conv2D(48, 3, activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(48, 3, padding='same', activation='relu'))
model.add(Conv2D(48, 3, activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(10, 3, activation='relu'))
model.add(GlobalAvgPool2D())
model.add(Activation('softmax'))

model.compile('adam', 'categorical_crossentropy', metrics=['accuracy'])


#
# Training
#

history = m.fit(x_train, y_train, batch_size=100, epochs=10, validation_split=0.1)


#
# Evaluation
#

print("\nTest-set:", m.evaluate(x_test, y_test))


#
# Saving
#

m.save_weights('weights.h5')
pickle.dump(history.history, open('history.p', 'wb'))

#
# Plotting
#
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.ylabel('Loss value')
plt.xlabel('Epochs')
plt.legend(['Loss', 'Validation loss'])
plt.show()
